//这里存放所有的方法类型

//轮播图
export const SET_SLIDERS = 'SET_SLIDERS'

// 用户登录
export const USER_LOGIN = 'USER_LOGIN';

// 设置用户信息
export const SET_USER = 'SET_USER';

// 设置已经获取权限了 
export const SET_PERMISSION = 'SET_PERMISSION';

// 验证是否登录过
export const USER_VALIDATE = 'USER_VALIDATE';
//验证退出登录
export const USER_LOGOUT = 'USER_LOGOUT';
// 设置路由权限【菜单权限】
export const SET_MENU_PERMISSION = 'SET_MENU_PERMISSION';
// 添加路由动作
export const ADD_ROUTE = 'ADD_ROUTE'

//创建ws
export const CREATE_WEBSOCKET = 'CREATE_WEBSOCKET'
// 设置ws
export const SET_MESSAGE = 'SET_MESSAGE'