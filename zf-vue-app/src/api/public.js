import config from '@/config/public';
import axios from '@/utils/request'

// 获取轮播图功能
export const getSlider = () => axios.get(config.getSlider);
//调用接口有两种方式
//1.在页面中直接调用
//2.vuex 获取数据 --actions  <=选择
//用第二种的好处：数据是全局的，复用性高，做缓存功能


// 创建一个唯一的用户标识 和  验证码对应上  1：1234
export const getCaptcha = () => axios.get(config.getCaptcha, {
    //通过获取本地会话可以得到不同的uid的值，也就是获取不同的验证码
      params: {
          uid: getLocal('uuid')
      }
  })