<a name="1CQhH"></a>
#### 了解HTML5 WebSocket
- WebSocket 是 HTML5 开始提供的一种在单个 TCP 连接上进行全双工通讯的协议。
- WebSocket 使得客户端和服务器之间的数据交换变得更加简单，允许服务端主动向客户端推送数据。在 WebSocket API 中，浏览器和服务器只需要完成一次握手，两者之间就直接可以创建持久性的连接，并进行双向数据传输。
- 在 WebSocket API 中，浏览器和服务器只需要做一个握手的动作，然后，浏览器和服务器之间就形成了一条快速通道。两者之间就直接可以数据互相传送。
- 现在，很多网站为了实现推送技术，所用的技术都是 Ajax 轮询。轮询是在特定的的时间间隔（如每1秒），由浏览器对服务器发出HTTP请求，然后由服务器返回最新的数据给客户端的浏览器。这种传统的模式带来很明显的缺点，即浏览器需要不断的向服务器发出请求，然而HTTP请求可能包含较长的头部，其中真正有效的数据可能只是很小的一部分，显然这样会浪费很多的带宽等资源。
- HTML5 定义的 WebSocket 协议，能更好的节省服务器资源和带宽，并且能够更实时地进行通讯。

浏览器通过 JavaScript 向服务器发出建立 WebSocket 连接的请求，连接建立以后，客户端和服务器端就可以通过 TCP 连接直接交换数据。
<a name="ORnfH"></a>
#### WebSocket封装
在src/utils中新建websocket.js进行封装<br />src/utils/websocket.js文件
```javascript
import {getLocal } from '@/utils/local'
import store from '../store'
import * as types from '@/store/action-types'
class WS {
    constructor(config = {}) {//配置项
        this.url = config.url || 'localhost'; 
        this.port = config.prot || 4000;
      //protocol是个只读属性，用于返回服务器端选中的子协议的名字；这是一个在创建WebSocket 对象时，在参数protocols中指定的字符串
        this.protocol = config.protocol || 'ws';
        // 心跳检测 证明你还能用 30秒
        this.time = config.time || 30 * 1000;
        this.ws = null;
    }
    onOpen = () => {
        //连接建立，即握手成功触发的事件
        // 规定好发的必须是对象 对象里包含两个字段 type  data
        // websocket 是基于tcp 第一次连接靠的http 但是不能修改header
        this.ws.send(JSON.stringify({//转化为字符串
            type:'auth',
            data: getLocal('token')
        }))
    }
    onMessage = (e) => {//收到服务器消息时触发的事件
        //console.log(e.data)
        let {type,data} = JSON.parse(e.data);
        
        switch(type){
            case 'noAuth':
                console.log('没有权限');
                break;
            case 'heartCheck':
                this.checkServer();//触发检查服务器的方法，为了防止断线
                this.ws.send(JSON.stringify({type:'heartCheck'}));
                break;
            default://默认情况下时提交消息
                store.commit(types.SET_MESSAGE,data);
        }

    }
    onError = () => {//异常触发的事件
        setTimeout(() => {
            this.create();//重新调用create方法 连线的 
        }, 1000);
    }
    onClose = () => {//关闭连接触发的事件
        this.ws.close();
    }
    create() { // webworker
        this.ws = new WebSocket(`${this.protocol}://${this.url}:${this.port}`);
        this.ws.onopen = this.onOpen;
        this.ws.onmessage = this.onMessage;
        this.ws.onclose = this.onclose;
        this.ws.onerror = this.onError
    }
    checkServer(){//服务器回应
        clearTimeout(this.timer); // 防抖
        this.timer = setTimeout(() => {
            this.onClose();
            this.onError();
        }, this.time + 1000); //断线重连
    }
    send = (msg)=>{//发送信息   src/views/manager/infoPublish.vue中
      //  <el-input placeholder="请输入要发送给用户的信息" v-model="msg"></el-input>就会接受到发送的信息
        this.ws.send(JSON.stringify(msg))//在Network上可以看到发送的信息
    }
}
export default WS;
```
在src/store/action-types.js中继续添加方法
```javascript
//创建ws
export const CREATE_WEBSOCKET = 'CREATE_WEBSOCKET'
// 设置ws
export const SET_MESSAGE = 'SET_MESSAGE'
```
src/router/hooks.js文件
```javascript
//...
export const createWebSockect = async function (to,from,next) {
    // 如果登陆了 但是没有创建websocket
    //store.state.ws是在src/store/rootModule.js中存值变量
    if(store.state.user.hasPermission && !store.state.ws){//调用创建ws方法
        store.dispatch(`${types.CREATE_WEBSOCKET}`);
        next();
    }else{//登录了并且创建ws
        next();
    }
}

export default {
    loginPermission,
    menuPermisson,
    createWebSockect
}
```
src/store/rootModule.js文件
```javascript
import { getSlider } from '../api/public';
import WS from '@/utils/websocket'
import * as types from './action-types';

console.log(types)
export default {
    state: {
        sliders: [],
        ws:null,
        message:''
    },
    mutations: {
        [types.SET_SLIDERS](state, payload) {
            state.sliders = payload;
        },
        [types.CREATE_WEBSOCKET](state,ws){//存放ws信息
            state.ws = ws
        },
        [types.SET_MESSAGE](state,msg){//存放msg信息
            state.message = msg
        }
    },
    actions: {
        async [types.SET_SLIDERS]({ commit }) {
            let { data } = await getSlider();
            commit(types.SET_SLIDERS, data)
        },
        async [types.CREATE_WEBSOCKET]({commit}){
            let ws = new WS();
            ws.create();//调用src/utils/websocket.js中create的方法
            commit(types.CREATE_WEBSOCKET,ws);
        }
    }
}
```
